# mature

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 前言

此專案透過 [vue-cli](https://cli.vuejs.org/) 建立


* 使用了裡面的 plugins
  - vue/cli-plugin-babel
  - vue/cli-plugin-pwa
  - vue/cli-plugin-router
  - vue/cli-plugin-vuex
  - vue/cli-service
  
* css 部分使用 sass 

* icon 部分使用 svg 轉 font
  - 轉換工具 - https://icomoon.io/
  - 把 /src/assets/font_svg/selection.json import 進去，就能有目前的專案的 icons
  - /src/assets/font_svg/*.svg 的檔案，都是目前最新的 icon 
  - css 在 _font.scss 裡面
  - 每個 icon 都有 20px*20px 的外框 

  ```html
  <div class="icon"><div class="icon-close"></div>
  ```
* 圖片部分，是使用我自己的壓縮工具或是此網站 [tinypng](https://tinypng.com/)

## 套件：
---

### [Hooper](https://github.com/baianat/hooper)

* 用於 `首頁廣告輪播` 和 `影片內頁廣告輪播`

* 這套件，已經 clone 下來在此專案中。
原因是： 它的 touchstart, touchmove, touchend 並沒有加上 event.stopPropagation()
當在滑動此套件時，會跟著滑動頁面，所以要加上去，因為其他的 api 其實都很足夠用，不需要到太複雜的功能，只唯獨欠缺那個判斷。
所以才決定 clone 下來到此專案中。

* 檔案在此專案中: `components/HoopCustomCopy.js`

### [Videojs](https://videojs.com/)

* 用於 `影片播放頁面`
* 設定方式可以看看 `views/VideoPage.vue` 裡面的 `videoOptions`

### [lodash](https://www.npmjs.com/package/lodash)

* 只有使用 [throttle](https://www.npmjs.com/package/lodash.throttle)

### service-worker

* 使用 vue-cli 建立出來的設定檔
  - vue.config.js
  - service-worker.js 
    - nginx 部分請設定成 no-cache
    - 檔案名稱切勿更改（假如已經上了正式機的話，目前沒有找到改了檔名的解法
  - registerServiceWorker.js
  - index.html 
    - 這隻檔案也請勿做 cache，為了做強制更新用
    - 這隻檔案目前沒有包含在 service-worker 裡面做 cache

      ```js
      // 排除設定方式，在 vue.config.js 中
      workboxOptions: {
        exclude: [/\.map$/, /\.html$/],
      },  
      ```
    - 裡面有個強制更新方式 （打開就能強制更新，但還是需要 window.reload 才行或是使用者更新後，在重開網頁。）

      ```js
      if(window.navigator && navigator.serviceWorker) {
        navigator.serviceWorker.getRegistrations()
        .then(function(registrations) {
          for(let registration of registrations) {
            registration.unregister();
          }
        });
      }
      ```
### [better-scroll 2.x](https://better-scroll.github.io/docs/zh-CN/guide/)

* 使用地方
  - 首頁
  - 收藏
  - 分類

* 使用插件
  - infinity
  - pull-up
  - scroll-bar

### mixin 裡面的 slideTouchViews.js

* 這是用來操作 左/右 swipe 用







