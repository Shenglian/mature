import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// 圖片底圖轉場用
Vue.directive('background-image',{
  bind: function (el, binding) {
    const img = document.createElement('img');
    img.src = binding.value
    img.remove()
    el.style.backgroundImage = `url(${binding.value})`  
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
