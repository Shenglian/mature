function normalizeChildren(context) {
  var slotProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  if (context.$scopedSlots.default) {
    return context.$scopedSlots.default(slotProps) || [];
  }

  return context.$slots.default || [];
}

const scrollTo = function (ele, to, duration) {
  const
    element = ele || document.scrollingElement || document.documentElement,
    start = element.scrollLeft,
    change = to - start,
    startDate = +new Date(),
    // t = current time
    // b = start value
    // c = change in value
    // d = duration
    easeInOutQuad = function (t, b, c, d) {
      t /= d / 2;
      if (t < 1) return c / 2 * t * t + b;
      t--;
      return -c / 2 * (t * (t - 2) - 1) + b;
    },
    animateScroll = function () {
      const currentDate = +new Date();
      const currentTime = currentDate - startDate;
      element.scrollLeft = parseInt(easeInOutQuad(currentTime, start, change, duration));
      if (currentTime < duration) {
        requestAnimationFrame(animateScroll);
      } else {
        element.scrollLeft = to;
      }
    };
  animateScroll();
};

const Carousel = {
  name: 'Poker',
  provide: function provied() {
    return {
      $poker: this
    }
  },
  data() {
    return {
      enabledSlideTouchViews: true,
      windowWidth: document.body.clientWidth,
      direction: null,
      pageX: null,
      pageY: null,
      fingerMoveY: 0,
      fingerMoveX: 0,
      currentIndex: 0,
      overWindowWidth: false,

      aaa: 0,
    }
  },

  mounted() {
    this.$refs.pokerSections.children[this.currentIndex].classList.add('on_mid')
    this.$refs.pokerSections.children[this.currentIndex + 1].classList.add('on_right')

    this.setX(this.$refs.pokerSections, 0);
  },


  methods: {
    touchstart(e) {
      const touches = e.touches[0];

      this.pageX = touches.pageX;
      this.pageY = touches.pageY;
    },

    touchmove(e) {
      const minValue = 5; // threshold
      const touches = e.touches[0];

      // 鎖縮放
      if (e.scale !== 1) event.preventDefault()

      // END - START
      let X = touches.pageX - this.pageX;
      let Y = touches.pageY - this.pageY;

      this.fingerMoveX = X
      this.fingerMoveY = Y

      if (!this.enabledSlideTouchViews) return

      // 要切換 section 不要滾動
      // if (Math.abs(this.fingerMoveX) > Math.abs(this.fingerMoveY)) {
      //   e.preventDefault();
      // }

      // 正在 scroll 當前 section 不要切換移動
      if (Math.abs(this.fingerMoveY) > Math.abs(this.fingerMoveX)) {
        return
      }

      // =>>
      if (this.fingerMoveX <= 0 && this.currentIndex >= this.$refs.pokerSections.children.length - 1) {
        return
      }

      // <<=
      if (this.fingerMoveX >= 0 && this.currentIndex <= 0) {
        return
      }

      if (this.currentIndex >= 1) {
        this.setX(this.$refs.pokerSections, X);
      } else {
        this.setX(this.$refs.pokerSections, X - (this.currentIndex) * this.windowWidth);
      }

      // console.log('touchmove', this.$refs)

      // this.setX(
      //   this.$refs.navBar, 
      //   this.$refs.nav.children[this.currentIndex].offsetLeft
      // );
    },

    touchend(e) {
      if (!this.enabledSlideTouchViews) return

      // 模拟是 click 状态
      if (this.fingerMoveX === 0) {
        return
      }

      if (Math.abs(this.fingerMoveY) > Math.abs(this.fingerMoveX)) {
        // 假如先橫向移動再直向移動，要把畫面移動回去
        this.setX(
          this.$refs.pokerSections, this.currentIndex * this.windowWidth * -1
        );
        return
      }

      this.$refs.pokerSections.style.transition = 'transform, .2s'
      this.$refs.pokerSections.style.transitionTimingFunction = 'cubic-bezier(0.165, 0.84, 0.44, 1)'

      if (
        this.fingerMoveX <= 0 &&
        Math.abs(this.fingerMoveX) > this.windowWidth * 0.2
      ) {
        this.direction = 'next'

        // 锁尾
        if (this.currentIndex < this.$refs.pokerSections.children.length - 1) {
          this.currentIndex++

          this.setX(
            this.$refs.pokerSections, 1 * this.windowWidth * -1
          );
        }
      } else if (
        this.fingerMoveX > 0 &&
        Math.abs(this.fingerMoveX) > this.windowWidth * 0.2
      ) {
        this.direction = 'prev'

        if (this.currentIndex > 0) {
          this.currentIndex--

          this.setX(
            this.$refs.pokerSections, 1 * this.windowWidth
          );
        }
      } else {
        this.setX(
          this.$refs.pokerSections, this.currentIndex * this.windowWidth * -1
        );
      }

      // reset value for detect click event
      this.fingerMoveX = 0
      this.fingerMoveY = 0
    },

    setX(el, x, unit) {
      el && (el.style.webkitTransform = 'translate3d(' + x + (unit || 'px') + ',0,0)');
    },

    onTransitionend() {
      for (let j = 0; j < this.$refs.pokerSections.children.length; j++) {
        this.$refs.pokerSections.children[j].classList.remove('on_mid')
        this.$refs.pokerSections.children[j].classList.remove('on_right')
        this.$refs.pokerSections.children[j].classList.remove('on_left')
      }

      this.$refs.pokerSections.style.transition = ''

      this.$refs.pokerSections.children[this.currentIndex].classList.add('on_mid')

      this.$refs.pokerSections.children[this.currentIndex - 1] ?
        this.$refs.pokerSections.children[this.currentIndex - 1].classList.add('on_left') :
        null

      this.$refs.pokerSections.children[this.currentIndex + 1] ?
        this.$refs.pokerSections.children[this.currentIndex + 1].classList.add('on_right') :
        null

      this.setX(this.$refs.pokerSections, 0);
    }
  },

  render: function (createElement) {
    let body = []

    const addons = this.$slots['hooper-addons'] || [];
    const _pokerList = pokerList.call(this, createElement)

    body = body.concat(addons, _pokerList)

    return createElement(
      'section', {
        attrs: {
          id: 'poker',
        },
        on: {
          touchstart: this.touchstart,
          touchmove: this.touchmove,
          touchend: this.touchend
        }
      },
      body
    )
  },
  props: {
    navs: {
      type: Array,
      required: true
    }
  }
}

const Navigation = {
  inject: ['$poker'],
  name: 'PokerNavigation',
  mounted() {
    this.isOverScreenWidth()
    this.setNavBar()

    this.$refs.navBar.style.transition = 'transform, .2s'
    this.$refs.navBar.style.transitionTimingFunction = 'cubic-bezier(0.165, 0.84, 0.44, 1)'
  },
  computed: {
    abc() {
      return this.$poker.currentIndex ? this.$refs.nav.children[this.$poker.currentIndex].offsetLeft : 0
    }
  },
  methods: {
    isOverScreenWidth() {
      this.$refs.nav.scrollWidth > this.windowWidth 
      ? (this.$refs.nav.classList.remove('isFlatten'), this.overWindowWidth = false)
      : (this.$refs.nav.classList.add('isFlatten'), this.overWindowWidth = true)
    },

    setNavBar() {
      this.$refs.navBar.style.width = `${this.$refs.nav.children[0].clientWidth}px`
    },


    goToSection(sectionIndex) {

      if (sectionIndex === this.$poker.currentIndex) return 

      this.$poker.$refs.pokerSections.style.transition = 'transform, .2s'

      if (!this.overWindowWidth) {
        scrollTo(
          this.$refs.nav,
          this.$refs.nav.children[sectionIndex].offsetLeft,
          200
        )
      }

      if (sectionIndex > this.$poker.currentIndex) {
        this.$poker.$refs.pokerSections.children[this.$poker.currentIndex + 1] ?
          this.$poker.$refs.pokerSections.children[this.$poker.currentIndex + 1].classList.remove('on_right') :
          null

        this.$poker.$refs.pokerSections.children[sectionIndex] ?
          this.$poker.$refs.pokerSections.children[sectionIndex].classList.add('on_right') :
          null

        this.$poker.setX(
          this.$poker.$refs.pokerSections, 1 * this.$poker.windowWidth * -1
        );
      } else {
        this.$poker.$refs.pokerSections.children[this.$poker.currentIndex - 1] ?
          this.$poker.$refs.pokerSections.children[this.$poker.currentIndex - 1].classList.remove('on_left') :
          null

        this.$poker.$refs.pokerSections.children[sectionIndex] ?
          this.$poker.$refs.pokerSections.children[sectionIndex].classList.add('on_left') :
          null

        this.$poker.setX(
          this.$poker.$refs.pokerSections, 1 * this.$poker.windowWidth
        );
      }

      this.$poker.setX(this.$refs.navBar, this.$refs.nav.children[sectionIndex].offsetLeft);
      this.$refs.navBar.style.transition = 'transform, .3s'

      this.$poker.currentIndex = sectionIndex
    }
  },
  render(h) {
    let navItems = []
    const navsLength = this.$poker.navs.length

    for (let i = 0; i < navsLength; i++) {
      navItems.push(
        h('div', {
          class: 'nav_items',
          on: {
            click: this.goToSection.bind(this, i)
          },
        }, this.$poker.navs[i])
      )
    }

    let navBar = h('div', {
      ref: 'navBar',
      style: `webkitTransform: translate3d(${this.abc}px, 0, 0)`,
      attrs: {
        id: 'nav_bar'
      }
    })

    return [
      h('nav', {
        class: 'nav',
        ref: 'nav',
      }, navItems.concat(navBar))
    ]  
  }
}

function pokerList(h) {
  let children = normalizeChildren(this)

  return [
    h(
      'div', {
        attrs: {
          id: 'poker_list'
        },
      }, [
        h('div', {
          ref: 'pokerSections',
          attrs: {
            id: 'poker_sections'
          },
          on: {
            transitionend: this.onTransitionend
          }
        }, children)
      ]
    )
  ]
}

export default Carousel;
export { Carousel as Poker, Navigation }