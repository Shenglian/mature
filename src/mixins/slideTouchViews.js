export default {
  data() {
    return {
      enabledSlideTouchViews: true,
      windowWidth: document.body.clientWidth,
      navWidth: document.body.clientWidth / 5,
      direction: null,
      pageX: null,
      pageY: null,
      fingerMoveY: 0,
      fingerMoveX: 0,
      currentIndex: 0,
    }
  },
  computed: {},
  mounted() {
    this.$refs.wrapperSections.children[this.currentIndex].classList.add('on_mid')
    this.$refs.wrapperSections.children[this.currentIndex + 1].classList.add('on_right')

    this.setX(this.$refs.wrapperSections, 0);
    this.setX(this.$refs.navbar, 0);
  },
  methods: {
    touchstart(e) {
      const touches = e.touches[0];

      this.pageX = touches.pageX;
      this.pageY = touches.pageY;
    },

    touchmove(e) {
      const minValue = 5; // threshold
      const touches = e.touches[0];

      // 鎖縮放
      if (e.scale !== 1) event.preventDefault()

      // END - START
      let X = touches.pageX - this.pageX;
      let Y = touches.pageY - this.pageY;

      this.fingerMoveX = X
      this.fingerMoveY = Y

      if (!this.enabledSlideTouchViews) return

      // 要切換 section 不要滾動
      // if (Math.abs(this.fingerMoveX) > Math.abs(this.fingerMoveY)) {
      //   e.preventDefault();
      // }

      // 正在 scroll 當前 section 不要切換移動
      if (Math.abs(this.fingerMoveY) > Math.abs(this.fingerMoveX)) {
        return 
      }

      // =>>
      if (this.fingerMoveX <= 0 && this.currentIndex >= this.$refs.wrapperSections.children.length - 1) {
        return
      }

      // <<=
      if (this.fingerMoveX >= 0 && this.currentIndex <= 0) {
        return 
      }
       
      if (this.currentIndex >= 1) {
        this.setX(this.$refs.wrapperSections, X);
      } else {
        this.setX(this.$refs.wrapperSections, X - (this.currentIndex) * this.windowWidth);
      }
      
      this.setX(this.$refs.navBar, (this.currentIndex) * this.navWidth - X * 0.1);
    },

    touchend(e) {
      if (!this.enabledSlideTouchViews) return
      
      // 模拟是 click 状态
      if (this.fingerMoveX === 0) {
        return
      }

      if (Math.abs(this.fingerMoveY) > Math.abs(this.fingerMoveX)) {
        // 假如先橫向移動再直向移動，要把畫面移動回去
        this.setX(
          this.$refs.wrapperSections, this.currentIndex * this.windowWidth * -1
        );
        return 
      }

      this.$refs.wrapperSections.style.transition = 'transform, .2s'
      this.$refs.navBar.style.transition = 'transform, .2s'
      this.$refs.wrapperSections.style.transitionTimingFunction = 'cubic-bezier(0.165, 0.84, 0.44, 1)'
      this.$refs.navBar.style.transitionTimingFunction = 'cubic-bezier(0.165, 0.84, 0.44, 1)'

      if (
        this.fingerMoveX <= 0 && 
        Math.abs(this.fingerMoveX) > this.windowWidth * 0.2
      ) {
        this.direction = 'next'

        // 锁尾
        if (this.currentIndex < this.$refs.wrapperSections.children.length - 1) {
          this.currentIndex++

          this.setX(
            this.$refs.wrapperSections, 1 * this.windowWidth * -1
          );

          this.setX(
            this.$refs.navBar, this.currentIndex * this.navWidth
          );
        }
      } else if (
        this.fingerMoveX > 0 && 
        Math.abs(this.fingerMoveX) > this.windowWidth * 0.2
      ) {
        this.direction = 'prev'

        if (this.currentIndex > 0) {
          this.currentIndex--

          this.setX(
            this.$refs.wrapperSections, 1 * this.windowWidth
          );

          this.setX(
            this.$refs.navBar, this.currentIndex * this.navWidth
          );
        }
      } else {
        this.setX(
          this.$refs.wrapperSections, this.currentIndex * this.windowWidth * -1
        );
      }

      // reset value for detect click event
      this.fingerMoveX = 0
      this.fingerMoveY = 0
    },

    setX(el, x, unit) {
      el && (el.style.webkitTransform = 'translate3d(' + x + (unit || 'px') + ',0,0)');
    },

    onTransitionend() {      
      for (let j = 0; j < this.$refs.wrapperSections.children.length; j++) {
        this.$refs.wrapperSections.children[j].classList.remove('on_mid')
        this.$refs.wrapperSections.children[j].classList.remove('on_right')
        this.$refs.wrapperSections.children[j].classList.remove('on_left')
      }

      this.$refs.wrapperSections.style.transition = ''

      this.$refs.wrapperSections.children[this.currentIndex].classList.add('on_mid')

      this.$refs.wrapperSections.children[this.currentIndex - 1] ?
        this.$refs.wrapperSections.children[this.currentIndex - 1].classList.add('on_left') :
        null

      this.$refs.wrapperSections.children[this.currentIndex + 1] ?
        this.$refs.wrapperSections.children[this.currentIndex + 1].classList.add('on_right') :
        null

      this.setX(this.$refs.wrapperSections, 0);
    },

    goToSection(sectionIndex) {
      this.$refs.wrapperSections.style.transition = 'transform, .2s'

      if (sectionIndex > this.currentIndex) {
        this.$refs.wrapperSections.children[this.currentIndex + 1] 
        ? this.$refs.wrapperSections.children[this.currentIndex + 1].classList.remove('on_right') 
        : null

        this.$refs.wrapperSections.children[sectionIndex] 
        ? this.$refs.wrapperSections.children[sectionIndex].classList.add('on_right')
        : null

        this.setX(
          this.$refs.wrapperSections, 1 * this.windowWidth * -1
        );
      } else {
        this.$refs.wrapperSections.children[this.currentIndex - 1] 
        ? this.$refs.wrapperSections.children[this.currentIndex - 1].classList.remove('on_left') 
        : null

        this.$refs.wrapperSections.children[sectionIndex] 
        ? this.$refs.wrapperSections.children[sectionIndex].classList.add('on_left')
        : null

        this.setX(
          this.$refs.wrapperSections, 1 * this.windowWidth
        );
      }

      this.setX(this.$refs.navBar, sectionIndex * this.navWidth);
      this.$refs.navBar.style.transition = 'transform, .3s'

      this.currentIndex = sectionIndex
    }
  }
}
