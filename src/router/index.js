import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Index from '@/views/Index.vue'
import VideoPage from '@/views/VideoPage.vue'
import Type from '@/views/Type.vue'
import typeMovieResult from '@/views/typeMovieResult.vue'
import typeStarResult from '@/views/typeStarResult.vue'
import Collection from '@/views/Collection.vue'
import Search from '@/views/Search.vue'
import User from '@/views/User.vue'
import Register from '@/views/Register.vue'
import Login from '@/views/Login.vue'
import ForgetPassword from '@/views/ForgetPassword.vue'
import Invite from '@/views/Invite.vue'

import Test from '@/views/Test.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/test',
    name: 'test',
    component: Test
  },
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/', name: 'index',
    component: Index,
    meta: { keepAlive: true, requireHeader: true, requireHeaderSearch: true, requireFooter: true }
  },
  {
    path: '/video/:videoId', name: 'videoPage',
    component: VideoPage,
    meta: { keepAlive: false, requireHeader: false, requireHeaderSearch: true, requireFooter: true }
  },
  {
    path: '/type', name: 'type',
    component: Type,
    meta: { keepAlive: false, requireHeader: true, requireHeaderSearch: true, requireFooter: true }
  },
  {
    path: '/type_movie/:movieId', name: 'typeMovieResult',
    component: typeMovieResult,
    meta: { keepAlive: true, requireHeader: false, requireHeaderSearch: true, requireFooter: true }
  },
  {
    path: '/type_stars/:starId', name: 'typeStarResult',
    component: typeStarResult,
    meta: { keepAlive: true, requireHeader: false, requireHeaderSearch: true, requireFooter: true }
  },
  {
    path: '/collection', name: 'collection',
    component: Collection,
    meta: { keepAlive: false, requireHeader: true, requireHeaderSearch: true, requireFooter: true }
  },
  {
    path: '/search', name: 'search',
    component: Search,
    meta: { keepAlive: false, requireHeader: false, requireFooter: true }
  },
  // 會員 - 註冊 登入 忘記密碼
  { path: '/user', name: 'user', 
    component: User,
    meta: { keepAlive: false, requireHeader: true, requireHeaderSearch: false, requireFooter: true }
  },
  { path: '/user/register', name: 'register', 
    component: Register,
    meta: { keepAlive: false, requireHeader: false, requireHeaderSearch: true, requireFooter: true }
  },
  { path: '/user/login', name: 'login', 
    component: Login,
    meta: { keepAlive: false, requireHeader: false, requireHeaderSearch: true, requireFooter: true }
  },
  { path: '/user/forgetPassword', name: 'forgetPassword', 
    component: ForgetPassword,
    meta: { keepAlive: false, requireHeader: false, requireHeaderSearch: true, requireFooter: true } 
  },
  { path: '/user/invite', name: 'invite', 
    component: Invite,
    meta: { keepAlive: false, requireHeader: false, requireHeaderSearch: true, requireFooter: false } 
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
