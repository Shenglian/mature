const types = {
  USER_LOGIN_IN: 'AuthService/USER_LOGIN_IN',
}

const state = {
  isAuth: false,
}

const getters = {
  isAuth: s => s.isAuth,
}

const actions = {
  login({ commit }) {
    commit(types.USER_LOGIN_IN)
  }
}

// mutations
const mutations = {
  [types.USER_LOGIN_IN] (s, d) {
    s.isAuth = true;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
