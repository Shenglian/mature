module.exports = {
  publicPath: process.env.NODE_ENV === 'production' && process.env.CI_PROJECT_NAME
  ? '/' + process.env.CI_PROJECT_NAME + '/'
  : '/',
  pwa: {
    name: '爱威奶 | 高清免费线上看',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    manifestOptions: {
      display: 'standalone',
      theme_color: '#333333',
      msTile_color: '#333333',
      start_url: process.env.CI_PROJECT_NAME ? '/' + process.env.CI_PROJECT_NAME + '/' : '/',
      scope: process.env.CI_PROJECT_NAME ? '/' + process.env.CI_PROJECT_NAME + '/' : '/',
    },
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      exclude: [/\.map$/, /\.html$/],
      swSrc: 'src/service-worker.js', 
    },
  }
}
